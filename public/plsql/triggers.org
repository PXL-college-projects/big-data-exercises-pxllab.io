#+TITLE: Oefeningen PL/SQL - Hoofdstuk 4: Triggers
#+AUTHOR: Robin Wils
#+INCLUDE: "../org-options.org"
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../solarized-dark.min.css"/>
-----
[[file:../index.org][Home]] > [[file:plsql.org][PLSQL]] > Triggers \\
[[https://pxl-college-projects.gitlab.io/big-data-exercises-pxl.gitlab.io/plsql/triggers.org][View org source]]

#+TOC: headlines

** Opgave 1
Zorg ervoor dat onderstaande controles automatisch gebeuren indien
het laagste en/of hoogste salaris wordt aangepast in de tabel jobs:

- als de gebruiker 'student' en/of 'bezoeker' deze aanpassing(en) 
  wil doen dan mag dit niet doorgaan en moet de melding "Je hebt 
  onvoldoende rechten om deze actie uit te voeren!" verschijnen.

- als een andere gebruiker deze aanpassing(en) wil doen dan mag 
  deze doorgaan maar moet volgende waarschuwing getoond worden "Ben 
  je zeker dat je het minimum en/of maximum salaris van één of 
  meerdere jobs wil aanpassen? Indien niet voer dan onmiddellijk een 
  ROLLBACK uit!" 

Bewaar als trigger_oef1.sql

#+BEGIN_SRC plsql
CREATE OR REPLACE TRIGGER salary_jobs_trigger
  BEFORE UPDATE OF min_salary, max_salary ON jobs
BEGIN
  IF USER IN ('STUDENT','BEZOEKER') 
  THEN 
    RAISE_APPLICATION_ERROR(
      -20000, 
      'Je hebt onvoldoende rechten om deze actie uit te voeren!'
    );
  ELSE
    DBMS_OUTPUT.PUT_LINE(
      'Ben je zeker dat je het minimum en/of maximum ' || 
      'salaris van één of meerdere jobs wil aanpassen? ' ||
      'Indien niet voer dan een ROLLBACK uit!'
    );
  END IF;
  END;
/
#+END_SRC
[[file:../sql-files/triggers/trigger_oef1.sql][sql bestand]]

Je kan triggers aanmaken via het volgende commando:
#+BEGIN_SRC
SQL> START <filepath>/trigger_naam
#+END_SRC

Je kan de trigger functie testen via:
#+BEGIN_SRC
SQL> UPDATE jobs set min_salary = 0 WHERE job_id = 'SA_REP';
#+END_SRC

** Opgave 2 
Schrijf een database trigger waardoor bijgehouden wordt wie, wanneer,
welke bewerking (insert, update of delete) op de tabel job_history 
heeft uitgevoerd. Maak daarvoor de tabel log_history aan met de 
volgende kolommen: log_user, log_datum, log_tijd, log_actie. \\
Bewaar als trigger_oef2.sql.

#+BEGIN_SRC plsql
CREATE TABLE log_history
(
  log_user VARCHAR2(30), 
  log_datum DATE, 
  log_tijd TIMESTAMP, 
  log_actie VARCHAR2(15)
);

CREATE OR REPLACE TRIGGER job_history_trigger
   AFTER INSERT OR UPDATE OR DELETE ON job_history
DECLARE
   v_action VARCHAR2(15) ;
BEGIN
  IF INSERTING THEN
    v_action := 'INSERT';
  ELSIF UPDATING THEN
    v_action := 'UPDATE';
  ELSE
    v_action := 'DELETE';
  END IF;
 
  INSERT INTO log_history
    VALUES(
      USER, 
      SYSDATE,
      SYSTIMESTAMP, 
      v_action
    );     
  END;
/
#+END_SRC

/(Ik denk dat je tijdens het examen oefening best de create table buiten 
het bestand doet, of in een aparte sql-file zou ook kunnen.)/

[[file:../sql-files/triggers/trigger_oef2.sql][sql bestand]]

Je kan de trigger functie testen via:
#+BEGIN_SRC
SQL> INSERT INTO job_history VALUES(127, '14-JAN-99', SYSDATE, 'ST_CLERK', 50);
SQL> SELECT * FROM job_history;
#+END_SRC

** Opgave 3 WERKT NIET
Schrijf een trigger die ervoor zorgt : 
- dat maandlonen enkel op maandag tussen 9:00 en 17:00 kunnen aangepast worden. 
- dat in het weekend geen employees verwijderd of toegevoegd kunnen worden.

Bewaar als trigger_oef3.sql 

_Tip:_ om de tijd uit de systeemdatum te halen gebruik je de functie to_char 
met als formaat 'hh24:mi:ss'.

/Denk er aan dat zondag dag 1 is in PLSQL./

#+BEGIN_SRC plsql
CREATE OR REPLACE TRIGGER month_loan_trigger
  BEFORE 
    UPDATE OF salary
    OR DELETE
    OR INSERT ON employees
DECLARE
  v_hour varchar2(8);
BEGIN
  IF DELETING OR INSERTING THEN
    IF 
      TO_CHAR(SYSDATE, 'D') = 7 
      OR TO_CHAR(SYSDATE, 'D') = 1 
    THEN
      RAISE_APPLICATION_ERROR(
        -20000, 
        'Can't add or delete salary during weekends.'
      );
    END IF;
  ELSE
    v_hour := TO_CHAR(SYSDATE,'hh24:mi:ss');
    IF
      TO_CHAR(SYSDATE, 'd') != 2 
      OR v_hour < '09:00:00' 
      OR v_hour > '17:00:00'
    THEN
      RAISE_APPLICATION_ERROR(
        -20000,  
        'Can't edit salary, ' || 
        'can only edit during Mondays during 9:00 - 17:00'
      );
    END IF;
  END IF;
  END;
/
#+END_SRC
[[file:../sql-files/triggers/trigger_oef3.sql][sql bestand]]

Je kan de trigger functie testen via:
#+BEGIN_SRC
SQL> DELETE FROM employees WHERE employee_id = 205;
SQL> INSERT INTO employees (salary) VALUES (10000);
#+END_SRC

/Het kan handig zijn om de systeemdatum van je PC tijdelijk te wijzigen./

[[file:../index.org][Home]] > [[file:plsql.org][PLSQL]] > Triggers
** Opgave 4
Doe al het nodige om er voor te zorgen dat bij het wijzigen van het salaris 
van een of meerdere medewerkers de verhoging nooit meer mag zijn dan 5%: 
- melding indien meer dan 5%: "Een loonsverhoging van meer dan 5% is niet 
  toegelaten" 
- de verhoging mag niet doorgaan Een verlaging resulteert in een foutmelding 
  "Loonsverlaging kan niet!" en deze mag dus ook niet doorgaan.

Bewaar als trigger_oef4.sql

#+BEGIN_SRC plsql
CREATE OR REPLACE TRIGGER loan_raise_trigger
  AFTER UPDATE 
  OF salary 
  ON employees
  FOR EACH ROW
BEGIN
  IF :NEW.salary < :OLD.salary THEN
    RAISE_APPLICATION_ERROR(-20000,'Loonsverlaging kan niet!');
  ELSIF :NEW.salary > :OLD.salary*1.05 THEN
    RAISE_APPLICATION_ERROR(
      -20000,
      'Een loonsverhoging van meer dan 5% is niet toegelaten!'
    );
  END IF;
  END;
/
#+END_SRC

[[file:../sql-files/triggers/trigger_oef4.sql][sql bestand]]

Je kan de trigger functie testen via:
#+BEGIN_SRC
SQL> UPDATE employees SET salary=100000 WHERE employee_id = 206;
#+END_SRC

** Opgave 5
Pas de oplossing van oefening 4 zodanig aan dat deze trigger enkel werkt als 
het gaat om de medewerkers die aangeworven zijn vóór 01‐01‐1995. \\
Bewaar als trigger_oef5.sql.

#+BEGIN_SRC plsql
CREATE OR REPLACE TRIGGER loan_raise_trigger
  AFTER UPDATE 
  OF salary 
  ON employees
  FOR EACH ROW
  WHEN (OLD.hire_date < TO_DATE('01-01-1995','dd-mm-yyyy'))
BEGIN
  IF :NEW.salary < :OLD.salary THEN
    RAISE_APPLICATION_ERROR(-20000,'Loonsverlaging kan niet!');
  ELSIF :NEW.salary > :OLD.salary*1.05 THEN
    RAISE_APPLICATION_ERROR(
      -20000,
      'Een loonsverhoging van meer dan 5% is niet toegelaten!'
    );
  END IF;
  END;
/
#+END_SRC

[[file:../sql-files/triggers/trigger_oef5.sql][sql bestand]]

Je kan de trigger functie testen via:
#+BEGIN_SRC
SQL> UPDATE employees SET salary=100000 WHERE employee_id = 204;
#+END_SRC

** Opgave 6
Doe al het nodige om er voor te zorgen dat: 
- bij het toevoegen of wijzigen in de tabel employees het job_id automatisch
  naar hoofdletters wordt geconverteerd en de naam en voornaam enkel begint 
  met een hoofdletter
- bij het toevoegen bij de hire_date nooit een datum in het verleden kan
  worden ingegeven
- medewerkers die promoveren en een managersfunctie krijgen, automatisch een 
  salarisverhoging krijgen van 5%. Het moet wel degelijk om een promotie gaan. 
  Iemand die president of vice president was en manager wordt, krijgt geen 
  verhoging. Gebruik hiervoor het job_id.

Sla op als trigger_oef6.sql.

#+BEGIN_SRC plsql
CREATE OR REPLACE TRIGGER data_validation_trigger
  BEFORE INSERT 
  OR UPDATE 
  ON employees
  FOR EACH ROW
BEGIN
  :NEW.job_id := UPPER(:NEW.job_id);
  :NEW.first_name := INITCAP(:NEW.first_name);
  :NEW.last_name := INITCAP(:NEW.last_name);
  IF INSERTING THEN
    IF :NEW.hire_date < SYSDATE THEN
      raise_application_error(
        -20000,
        'Date can''t be in the past.'
      );
    END IF;
  END IF;
  IF UPDATING THEN
    IF 
      :NEW.job_id IN ('%MAN', '%MGR') 
      AND :OLD.job_id NOT IN ('AD_PRES', 'AD_VP') 
    THEN
      :NEW.salary := :OLD.salary*1.05;
    END IF;
  END IF;
  END;
/
#+END_SRC

[[file:../sql-files/triggers/trigger_oef6.sql][sql bestand]]

Je kan de trigger functie testen via:
#+BEGIN_SRC
SQL> INSERT INTO employees (hire_date) 
VALUES(TO_DATE('2003/05/03', 'yyyy/mm/dd'));
#+END_SRC

Naast dit kan je ook andere delen van de trigger testen via functies.
Je kan een select gebruiken om de rijen op te vragen.

** Opgave 7 NIET DEFTIG GETEST

Zorg ervoor dat bij de creatie van een nieuwe employee volgende controles 
gebeuren: 
- als geen manager_id wordt ingevoerd, wordt automatisch de manager van het
  departement waar deze persoon aan toegewezen wordt, de directe chef. Toon 
  in dat geval als melding op het scherm "de chef wordt [de naam en de 
  voornaam van de chef]."
- als er geen hire_date werd ingevuld dan wordt dit automatisch de 
  eerstvolgende maandag na de huidige datum. 
- als geen salary werd opgegeven, wordt dit automatisch 1000. Druk in dat 
  geval af "het salaris van employee [naam employee] wordt 1000".

Als een employee verwijderd wordt, verschijnt een melding 
"dit is per maand een besparing van [/xxx/] euro", (waarbij je de /xxx/ 
vervangtdoor het maandloon van deze employee). \\
Bewaar als trigger_oef7.sql.

#+BEGIN_SRC plsql
CREATE OR REPLACE TRIGGER data_validation_trigger
  BEFORE 
    INSERT OR DELETE 
           ON employees
           FOR EACH ROW
  DECLARE
    v_manager_id departments.manager_id%TYPE;
    v_firstname employees.first_Name%TYPE;
    v_name employees.last_name%TYPE;
 
  BEGIN
  IF INSERTING THEN
    IF :NEW.manager_id IS NULL THEN
      SELECT manager_id
      INTO v_manager_id
      FROM departments
      WHERE department_id = :NEW.department_id;
      
      SELECT first_name, last_name
      INTO v_firstname, v_name
      FROM employees
      WHERE employee_id = v_manager_id;

      :NEW.manager_id := v_manager_id;
      DBMS_OUTPUT.PUT_LINE(
        'de chef wordt ' ||
        v_name || 
        ' ' || 
        v_firstname
      );
    END IF;

    IF :NEW.hire_date IS NULL THEN
      :NEW.hire_date := NEXT_DAY(SYSDATE, 'MONDAY');
    END IF;
 
    IF :NEW.salary IS NULL THEN
      :NEW.salary := 1000;
      DBMS_OUTPUT.PUT_LINE(
        'het salaris van employee ' || 
        :NEW.last_name || 
        ' wordt 1000.'
      );
    END IF;
 
  ELSIF DELETING THEN
    DBMS_OUTPUT.PUT_LINE(
      'dit is per maand een besparing van ' || 
      :OLD.salary || 
      ' euro.'
    );
  END IF;
  END;
/
#+END_SRC

[[file:../sql-files/triggers/trigger_oef7.sql][sql bestand]]

Je kan de trigger functie testen via:
#+BEGIN_SRC
SQL> DELETE FROM employees WHERE employee_id = 206;
#+END_SRC

[[file:../index.org][Home]] > [[file:plsql.org][PLSQL]] > Triggers
