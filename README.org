#+TITLE: README
#+AUTHOR: Robin Wils
#+OPTIONS: num:nil, ^:nil

* Big Data exercises PXL
Big Data exercises - This is a purely educational project which contains
solutions for PL/SQL and MongoDB exercises. \\
The exercises are written in Dutch.

** Big Data exercises 
Read it:
- on the [[https://pxl-college-projects.gitlab.io/big-data-exercises-pxl.gitlab.io][website]] (recommended)
- on [[https://gitlab.com/PXL-college-projects/big-data-exercises-pxl.gitlab.io/blob/master/public/index.org][gitlab]]

** Contribute
Do you feel like I missed something? \\
Feel free to create a pull request or a issue.

You will need: 
- GNU Emacs
- Org mode from MELPA
- The [[https://www.emacswiki.org/emacs/plsql.el][plsql.el]] package for syntax highlighting (save it as an elisp file and use M-x load-file to load the package)
- The [[https://github.com/krisajenkins/ob-mongo][ob-mongo]] package for syntax highlighting (save it as an elisp file and use M-x load-file to load the package)

